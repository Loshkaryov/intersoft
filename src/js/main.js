$(document).ready(function () {
	"use strict";

	/**
	 * header
	 */
   function topBg(){
      $(window).scroll(function() {
			$('.navbar-collapse').collapse('hide');
			var top = $(".top");
			var topHeight = top.height();
			var winTop = $(window).scrollTop();

         if(winTop >= topHeight + 2){
				top.addClass("top--scrolled");
         } else {
				top.removeClass("top--scrolled");
         }
       });
    }
    topBg();

	/**
	* scroll to place
	*/
   $(".js-scroll-down").click(function() {
     $("html, body").animate({scrollTop: $($(this).closest('section').next('section')).offset().top - 70 + "px"}, {duration: 700});
     return false;
   });

	/**
	 * spollers
	 */
	$('.delivery__mobil-title').each(function(){
		var self = $(this);
		self.on('click', function(){
			self.closest('.delivery__item').toggleClass('show');
		});
	});

	/**
	 * slider
	 */
	$('.brands__slider').slick({
		dots: false,
		arrows:false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		variableWidth: true
	});

});